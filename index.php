<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>


    <h1>Category Signup</h1>
    <div class="container-fluid">
        <form method="POST" action="conex.php">
            <div class="form-group">
                <label for="name"> Name</label>
                <input id="name" class="form-control" type="text" name="name">
            </div>
            <div class="form-group">
                <label for="category">description</label>
                <input id="description" class="form-control" type="text" name="description">
            </div>
            <button class="btn btn-primary" type="submit">Sign up</button>
        </form>
    </div>


    <div class="container">
        <?php require('conex.php') ?>
        <h1>List of category</h1>
        <table class="table table-light">
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Delete</th>
            </tr>
            <tbody>
                <?php
                // loop users
                foreach ($categorys as $category) {
                    echo "<tr><td>" . $category[0] . "</td><td>" . $category[1] . "</td>
                    <td><a href='delete.php?description=". $category[1]."'>delete</a> </td></tr>";
                }
                ?>
            </tbody>
        </table>
        <?php
        ?>
</body>

</html>